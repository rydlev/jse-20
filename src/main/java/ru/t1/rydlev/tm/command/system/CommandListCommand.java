package ru.t1.rydlev.tm.command.system;

import ru.t1.rydlev.tm.api.model.ICommand;
import ru.t1.rydlev.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandListCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public String getArgument() {
        return "-c";
    }

    @Override
    public String getDescription() {
        return "Show developer commands.";
    }

    @Override
    public String getName() {
        return "commands";
    }

}
