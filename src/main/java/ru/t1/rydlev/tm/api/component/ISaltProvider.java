package ru.t1.rydlev.tm.api.component;

public interface ISaltProvider {

    Integer getPasswordIteration();

    String getPasswordSecret();

}
