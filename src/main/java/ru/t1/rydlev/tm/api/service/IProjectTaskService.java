package ru.t1.rydlev.tm.api.service;

import ru.t1.rydlev.tm.model.Task;

public interface IProjectTaskService {

    Task bindTaskToProject(String userId, String projectId, String taskId);

    void removeProjectById(String userId, String projectId);

    void removeProjects();

    Task unbindTaskFromProject(String userId, String projectId, String taskId);

}
