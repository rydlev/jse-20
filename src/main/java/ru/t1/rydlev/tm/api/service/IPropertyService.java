package ru.t1.rydlev.tm.api.service;

import ru.t1.rydlev.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    String getApplicationVersion();

    String getAuthorEmail();

    String getAuthorName();

}
