package ru.t1.rydlev.tm.exception.field;

import ru.t1.rydlev.tm.exception.AbstractException;

public class AbstractFieldException extends AbstractException {

    public AbstractFieldException() {
    }

    public AbstractFieldException(String s) {
        super(s);
    }

    public AbstractFieldException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public AbstractFieldException(Throwable throwable) {
        super(throwable);
    }

    public AbstractFieldException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }

}
